import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '../components/pages/IndexPage'
import DataLeaflet from '../components/views/DataLeaflet'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: {
        template: '<router-view></router-view>'
      },
      children: [
        {
          path: '/',
          component: IndexPage,
          children: [{
            path: '',
            name: 'IndexPage',
            components: {
              mainView: IndexPage,
              success: DataLeaflet
            }
          },
          {
            path: '/googleMap',
            name: 'DataLeaflet',
            components: {
              success: DataLeaflet
            }
          }]
        }
      ]
    }
  ],
  linkActiveClass: 'active'
})

export default router

router.addRoutes([{
  path: '/',
  component: IndexPage
}])
