
export default new function () {
  const services = {
    leaflet: {
      loadData: '/leaflet/getCoordinate'
    }
  }

  const floatValidation = /^-?\d*(\.\d+)?$/g

  const digit = /\d/g

  this.checkFloatNumber = function () {
    return floatValidation
  }

  this.getDigit = function () {
    return digit
  }

  this.linkTo = function () {
    return services.leaflet
  }
}()
