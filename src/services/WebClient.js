import Axios from 'axios'

export default new function () {
  const apiPrefix = ''

  function handleError (status, message, callback) {
    if (callback != null) {
      callback(status, message)
    }
  }

  var request = function (method, data, url, onOk, onError) {
    var req = apiPrefix + url
    const getPromise = method(req, data)
    getPromise.then(response => {
      var obj = response.data
      if (obj !== undefined) {
        onOk(obj)
      }
      else {
        handleError(obj.status, obj.Message, onError)
      }
    }).catch(err => {
      handleError(-1, err, onError)
    })
    return getPromise
  }

  this.get = function (...args) {
    return request(Axios.get, null, ...args)
  }
  this.getWithParams = function (url, data, ...args) {
    return request(Axios.get, data, url, ...args)
  }
  this.post = function (url, data, ...args) {
    return request(Axios.post, data, url, ...args)
  }
  this.put = function (url, data, ...args) {
    return request(Axios.put, data, url, ...args)
  }
  this.delete = function (...args) {
    return request(Axios.delete, null, ...args)
  }
}()
