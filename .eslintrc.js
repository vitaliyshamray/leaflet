module.exports = {
  extends: [
    // add more generic rulesets here, such as:
    // 'eslint:recommended',
    'standard',
    // 'plugin:vue/essential'
    'plugin:vue/recommended'
  ],
  parserOptions: {
    parser: "babel-eslint",
    sourceType: 'module',
    allowImportExportEverywhere: true
  },
  rules: {
    "no-unused-vars": "off",
    "quotes": ["error",
      "single",
      {
        "avoidEscape": true,
        "allowTemplateLiterals": true
      }
    ],
    indent: 'off',
    'vue/html-self-closing': ['error', {
      'html': {
        'void': 'always'
      }
    }],
    'vue/max-attributes-per-line': 'off',
    'brace-style': ["error", "stroustrup", {
      "allowSingleLine": true
    }]
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
  },
  settings: {}
}
